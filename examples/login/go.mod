module gitlab.com/silas.ss/go-whatsapp/examples/login

require (
	github.com/Baozisoftware/qrcode-terminal-go v0.0.0-20170407111555-c0650d8dff0f
	gitlab.com/silas.ss/go-whatsapp v0.0.0
)

replace gitlab.com/silas.ss/go-whatsapp => ../../
